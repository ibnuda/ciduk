.PHONY: rilis rilis64 clean all
rilis:
	flutter build apk
	mv ./build/app/outputs/apk/release/app-release.apk ./ciduk.apk
rilis64:
	flutter build apk --target-platform android-arm64
	mv ./build/app/outputs/apk/release/app-release.apk ./ciduk64.apk
clean:
	flutter clean
unggah:
	upload.sh ciduk.apk
	upload.sh ciduk64.apk
all: rilis rilis64 unggah
