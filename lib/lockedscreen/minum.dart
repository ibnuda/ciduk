import 'dart:core';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:native_widgets/native_widgets.dart';

import 'package:ciduk/model.dart';
import 'package:ciduk/globals.dart' as globals;

class Minum extends StatelessWidget {
  final Pelanggan pelanggan;

  const Minum({Key key, this.pelanggan}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${pelanggan.nama}"),
      ),
      body: BorangMinum(pelanggan: pelanggan),
    );
  }
}

class BorangMinum extends StatefulWidget {
  final Pelanggan pelanggan;

  const BorangMinum({Key key, this.pelanggan}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new BorangMinumState(pelanggan);
}

class BorangMinumState extends State<BorangMinum> {
  final Pelanggan pelanggan;
  CatatAir catatair;
  final formkey = GlobalKey<FormState>();
  final scaffoldkey = GlobalKey<ScaffoldState>();

  TextEditingController kontrolerSampai;

  @override
  initState() {
    super.initState();
    kontrolerSampai = TextEditingController();
  }

  Future<Null> catatAir(sudahcatat) async {
    final form = formkey.currentState;
    if (form.validate()) {
      form.save();
      var payload = jsonEncode({'sampai': this.catatair.sampai});
      if (sudahcatat) {
        globals.Utility.putSomething(
            '/petugas/air/${this.catatair.nomormeteran}',
            globals.petugas.token,
            payload);
      } else {
        globals.Utility.postSomething(
            '/petugas/air/${this.catatair.nomormeteran}',
            globals.petugas.token,
            payload);
      }
      //Navigator.pop(context);
      //Navigator.of(context).pop();
      Navigator.of(context).pushReplacementNamed('/home');
    }
  }

  BorangMinumState(this.pelanggan);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("${pelanggan.nama}"),
        ),
        body: Form(
          key: formkey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                title: TextFormField(
                  decoration: InputDecoration(labelText: 'Nomor Meteran'),
                  initialValue: pelanggan.nomormeteran,
                  enabled: false,
                  keyboardType: TextInputType.text,
                  style: TextStyle(color: Colors.black),
                  autocorrect: false,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Harap isi sesuatu';
                    }
                  },
                ),
              ),
              ListTile(
                title: TextFormField(
                  decoration: InputDecoration(labelText: 'Tahun'),
                  initialValue: new DateTime.now().year.toString(),
                  enabled: false,
                  keyboardType: TextInputType.numberWithOptions(signed: true),
                  style: TextStyle(color: Colors.black),
                  autocorrect: false,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Harap isi sesuatu';
                    }
                  },
                ),
              ),
              ListTile(
                title: TextFormField(
                  decoration: InputDecoration(labelText: 'Bulan'),
                  initialValue: new DateTime.now().month.toString(),
                  enabled: false,
                  keyboardType: TextInputType.numberWithOptions(
                    signed: true,
                  ),
                  style: TextStyle(color: Colors.black),
                  autocorrect: false,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Harap isi sesuatu';
                    }
                  },
                ),
              ),
              ListTile(
                title: TextFormField(
                  decoration: InputDecoration(labelText: 'Sudah Tercatat'),
                  initialValue: pelanggan.sudahcatat ? 'sudah' : 'belum',
                  enabled: false,
                  keyboardType: TextInputType.numberWithOptions(
                    signed: true,
                  ),
                  style: TextStyle(color: Colors.black),
                  autocorrect: false,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Harap isi sesuatu';
                    }
                  },
                ),
              ),
              ListTile(
                title: TextFormField(
                  decoration: InputDecoration(labelText: 'Gunakan Sampai'),
                  initialValue: pelanggan.sudahcatat ? '' : '0',
                  keyboardType: TextInputType.number,
                  style: TextStyle(color: Colors.black),
                  // controller: kontrolerSampai,
                  autocorrect: false,
                  onSaved: (String val) {
                    var i = int.tryParse(val);
                    this.catatair = CatatAir(i, pelanggan.nomormeteran);
                  },
                  validator: (value) {
                    var v = int.tryParse(value);
                    if (v == null) {
                      return 'Harap isi angka.';
                    }
                    if (value.isEmpty) {
                      return 'Harap isi sesuatu';
                    }
                  },
                ),
              ),
              ListTile(
                  title: NativeButton(
                child: Text(
                  'Catat!',
                  style: TextStyle(color: Colors.white),
                ),
                minWidthAndroid: 220.0,
                buttonColor: Colors.blue,
                paddingExternal: const EdgeInsets.all(10.0),
                paddingInternal: const EdgeInsets.all(10.0),
                onPressed: () => catatAir(pelanggan.sudahcatat),
              )),
            ],
          ),
        ));
  }
}
