import 'dart:async';

import 'package:flutter/material.dart';

import 'package:ciduk/model.dart';
import 'package:ciduk/globals.dart' as globals;
import 'package:ciduk/lockedscreen/minum.dart';

class Home extends StatefulWidget {
  final Future<DaftarPelanggan> daftarpelanggan;

  const Home({Key key, this.daftarpelanggan}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

// State for managing fetching name data over HTTP
class _HomeState extends State<Home> {
  Future<DaftarPelanggan> dp;
  @override
  void initState() {
    if (globals.petugas == null) {
      Navigator.pushReplacementNamed(context, '/login');
    } else {
      super.initState();
      this.dp = anu();
    }
  }

  Future<DaftarPelanggan> anu() async =>
      globals.Utility.getSomething('/petugas/air')
          .then(jsonToDaftarPelanggan)
          .catchError((embuh) => []);

  final GlobalKey<RefreshIndicatorState> _refindkey =
      new GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text(
          'Daftar Pelanggan',
          textScaleFactor: globals.textScaleFactor,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: () => Navigator.pushNamed(context, '/home'),
          )
        ],
      ),
      drawer: globals.Utility.getMenuBar(context),
      body: WillPopScope(
          onWillPop: () => Future.value(false),
          child: FutureBuilder(
            future: this.dp,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Center(child: CircularProgressIndicator());
                default:
                  return _buildPelangganList(snapshot.data.pelanggan);
              }
            },
          )),
    );
  }

  Widget _buildPelangganList(something) {
    return new ListView.builder(
      itemBuilder: (context, i) {
        return _PelangganListItem(context, something[i]);
      },
      itemCount: something.length,
    );
  }

  Widget detailPelanggan(Pelanggan pelanggan) {
    return new Flexible(
        child: new Container(
      color: pelanggan.sudahcatat ? Colors.green : Colors.red,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          textContainer(pelanggan.nama, Colors.blue[400]),
          textContainer(pelanggan.telepon, Colors.blueGrey[400]),
          textContainer(pelanggan.alamat, Colors.blueGrey[400]),
        ],
      ),
      margin: EdgeInsets.only(left: 20.0),
    ));
  }

  Widget textContainer(String string, Color color) {
    if (string == null) {
      string = 'kosong';
    }
    return new Container(
      child: new Text(
        string,
        style: TextStyle(
            color: color, fontWeight: FontWeight.normal, fontSize: 16.0),
        textAlign: TextAlign.start,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
      ),
      margin: EdgeInsets.only(bottom: 10.0),
    );
  }
}

class _PelangganListItem extends ListTile {
  _PelangganListItem(context, Pelanggan pelanggan)
      : super(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BorangMinum(
                            pelanggan: pelanggan,
                          )));
            },
            leading:
                pelanggan.sudahcatat ? Icon(Icons.check) : Icon(Icons.edit),
            title: Text(
                'Nama: ${pelanggan.nama}\nMeteran: ${pelanggan.nomormeteran}'),
            subtitle: Text(pelanggan.alamat));
}
