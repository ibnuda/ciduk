library globals;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:native_widgets/native_widgets.dart';

import 'main.dart';

double textScaleFactor = 1.0;
String errorMessage = "";

String apiURL = "http://sumur.siskam.link";

class Petugas {
  final String nama, telepon;
  final int idgrup;
  String token;
  Petugas({this.token, this.nama, this.telepon, this.idgrup});

  @override
  String toString() {
    return "$nama $telepon".toString();
  }
}

Petugas petugas;

class Utility {
  static void showAlertPopup(
      BuildContext context, String title, String detail) async {
    void showDemoDialog<T>({BuildContext context, Widget child}) {
      showDialog<T>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => child,
      );
    }

    return showDemoDialog<Null>(
        context: context,
        child: NativeDialog(
          title: title,
          content: detail,
          actions: <NativeDialogAction>[
            NativeDialogAction(
                text: 'OK',
                isDestructive: false,
                onPressed: () {
                  Navigator.pop(context);
                }),
          ],
        ));
  }

  static Future<String> getSomething(String path) async {
    var requrl = apiURL + path;
    HttpClient client = new HttpClient();
    var token = petugas.token;
    var res;
    var req = await client.getUrl(Uri.parse(requrl));
    req.headers.set('authorization', 'Bearer $token');
    req.headers.set('content-type', 'application/json');
    var resp = await req.close();
    try {
      String reply = await resp.transform(utf8.decoder).join();
      res = reply;
    } catch (ex) {
      res = ex.toString();
    }
    client.close();
    print('getSomething');
    return res;
  }

  static Future<String> postSomething(
      String path, String token, String payload) async {
    print(payload);
    var requrl = apiURL + path;
    HttpClient client = new HttpClient();
    var res;
    var req = await client.postUrl(Uri.parse(requrl));
    req.headers.set('content-type', 'application/json');
    if (token != null) {
      req.headers.set('authorization', 'Bearer $token');
    }
    req.add(utf8.encode(payload));
    var resp = await req.close();
    try {
      String reply = await resp.transform(utf8.decoder).join();
      res = reply;
    } catch (exception) {
      res = 'eror ngepost';
    }
    client.close();
    print('postSomething');
    return res;
  }

  static Future<String> putSomething(
      String path, String token, String payload) async {
    var requrl = apiURL + path;
    print(payload);
    HttpClient client = new HttpClient();
    var res;
    var req = await client.putUrl(Uri.parse(requrl));
    req.headers.set('content-type', 'application/json');
    if (token != null) {
      req.headers.set('authorization', 'Bearer $token');
    }
    req.add(utf8.encode(payload));
    var resp = await req.close();
    try {
      String reply = await resp.transform(utf8.decoder).join();
      res = reply;
    } catch (exception) {
      res = 'Error Getting Data';
    }
    client.close();
    print('putSomething');
    return res;
  }

  static Widget getMenuBar(BuildContext context) {
    return Drawer(
      child: SafeArea(
        child: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.account_circle),
              title: Text(
                petugas.nama,
                textScaleFactor: textScaleFactor,
                maxLines: 1,
              ),
              subtitle: Text(
                petugas.telepon,
                textScaleFactor: textScaleFactor,
                maxLines: 1,
              ),
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.arrow_back),
              title: Text(
                'Keluar',
                textScaleFactor: textScaleFactor,
              ),
              onTap: () {
                appAuth.logout();
                appAuth.reset();
                //Navigator.of(context).pushNamed("/login");
                Navigator.pushReplacementNamed(context, '/login');
              },
            )
          ],
        ),
      ),
    );
  }
}
