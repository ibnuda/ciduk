import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:ciduk/globals.dart' as globals;
import '../main.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.prefs});

  final SharedPreferences prefs;

  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String _status = 'no-action';
  String _telepon, _password;

  final formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _controllerUsername, _controllerPassword;

  @override
  initState() {
    String tempUsername = "";
    _controllerUsername = TextEditingController(text: tempUsername);
    _controllerPassword = TextEditingController();

    super.initState();
    print(_status);
  }

  Future<Null> _submit() async {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      final snackbar = SnackBar(
        duration: Duration(seconds: 30),
        content: Row(
          children: <Widget>[
            NativeLoadingIndicator(),
            Text("  Memasukkan bola ke gawang...")
          ],
        ),
      );
      _scaffoldKey.currentState.showSnackBar(snackbar);

      setState(() => this._status = 'memuat');
      appAuth
          .store(_telepon.toString().toLowerCase().trim(),
              _password.toString().trim())
          .then((onValue) {
        appAuth.login().then((result) {
          if (result) {
            Navigator.of(context).pushReplacementNamed('/home');
          } else {
            setState(() => this._status = 'ditolak');
            globals.Utility.showAlertPopup(
                context, 'Info', globals.errorMessage);
          }
          _scaffoldKey.currentState.hideCurrentSnackBar();
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SafeArea(
        child: ListView(
          physics: AlwaysScrollableScrollPhysics(),
          key: PageStorageKey("Divider 1"),
          children: <Widget>[
            SizedBox(
              height: 220.0,
              child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Icon(
                    Icons.person,
                    size: 175.0,
                  )),
            ),
            Form(
              key: formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    title: TextFormField(
                      initialValue: "petugas",
                      decoration: InputDecoration(labelText: 'Nomor Telepon'),
                      validator: (val) =>
                          val.length < 1 ? 'Nomor telepon harus ada' : null,
                      onSaved: (val) => _telepon = val,
                      obscureText: false,
                      keyboardType: TextInputType.text,
                      // controller: _controllerUsername,
                      autocorrect: false,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                  ListTile(
                    title: TextFormField(
                      initialValue: "petugasa",
                      decoration: InputDecoration(labelText: 'Password'),
                      validator: (val) =>
                          val.length < 1 ? 'Harap masukkan kata sandi' : null,
                      onSaved: (val) => _password = val,
                      obscureText: true,
                      // controller: _controllerPassword,
                      keyboardType: TextInputType.text,
                      autocorrect: false,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ],
              ),
            ),
            ListTile(
              title: NativeButton(
                child: Text(
                  'Masuk',
                  textScaleFactor: globals.textScaleFactor,
                  style: TextStyle(color: Colors.white),
                ),
                minWidthAndroid: 220.0,
                buttonColor: Colors.blue,
                paddingExternal: const EdgeInsets.all(10.0),
                paddingInternal: const EdgeInsets.all(10.0),
                onPressed: _submit,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
