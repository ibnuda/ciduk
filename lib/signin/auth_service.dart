import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ciduk/globals.dart' as globals;
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';

class PetugasReq {
  final String telepon, password;
  PetugasReq({this.telepon, this.password});
  Map toJson() {
    return {'nomor_telepon': this.telepon, 'password': this.password};
  }
}

class AuthService {
  // Login
  Future<bool> login() async {
    var prefs = await SharedPreferences.getInstance();
    String _telepon = prefs.getString('telepon');
    String _password = prefs.getString('password');
    if (_telepon == null || _password == null) return false;

    // Get New Token
    String data =
        jsonEncode(PetugasReq(password: _password, telepon: _telepon));
    String _result = await globals.Utility.postSomething('/masuk', null, data);
    String _nama, _telp, _token;
    int _idgrup;
    try {
      Map decoded = json.decode(_result);

      if (decoded['errors'] != null) {
        globals.errorMessage = decoded['errors'].toString();
        return false;
      }
      _nama = decoded['nama'];
      _telp = decoded['telp'];
      _idgrup = decoded['id_grup'];
      _token = decoded['token'];

      globals.petugas = globals.Petugas(
        nama: _nama,
        telepon: _telp,
        idgrup: _idgrup,
        token: _token,
      );
    } catch (exception) {
      print("Error Decoding Data (login): $exception");
    }

    bool validToken = _token == null || _token.isEmpty ? false : true;
    if (!validToken) return false;
    return validToken;
  }

  Future<void> logout() async {
    globals.petugas = null;
    return;
  }

  Future<bool> sudahMasuk() async {
    if (globals.petugas == null) {
      return false;
    }
    var something = await globals.Utility.getSomething('/tarif');
    Map decoded = json.decode(something);
    if (decoded.toString().toLowerCase().contains('errors')) {
      globals.errorMessage = "" + decoded[0]['Description'].toString();
      return false;
    } else {
      return true;
    }
  }

//Future<String> getToken() async {
//  globals.petugas?.token = "";
//  String token = "";
//  var prefs = await SharedPreferences.getInstance();
//  String _telepon = prefs.getString('telepon');
//  String _password = prefs.getString('password');
//  if (_telepon == null || _password == null) return "";

//  // Get New Token
//  var mapData = Map();
//  mapData["telepon"] = "" + _telepon;
//  mapData["password"] = "" + _password;
//  String _result = await globals.Utility.getData(null);
//  try {
//    Map decoded = json.decode(_result);
//    if (decoded.toString().toLowerCase().contains('error')) {
//      globals.errorMessage = "" + decoded[0]['Description'].toString();
//      return "";
//    } else {
//      token = decoded['data']['Token'].toString() ?? "";
//    }
//  } catch (exception) {
//    print("Error Decoding Data (token): $exception");
//  }
//  globals.petugas?.token = token;

//  return token;
//}

//Future<bool> checkToken() async {
//  String resp = await globals.Utility.getData(null);
//  print("Response: " + resp);
//  try {
//    if (resp.contains('salah')) {
//      return false;
//    } else if (resp.contains('atau')) {
//      return false;
//    }
//  } catch (exception) {
//    print("Error Decoding Data, Token: $exception");
//  }
//  return true;
//}

  Future<void> store(String telepon, String password) async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('telepon', telepon);
    prefs.setString('password', password);
    return;
  }

  Future<void> reset() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('telepon', null);
    prefs.setString('password', null);
    return;
  }

//Future<bool> avaliable() async {
//  var prefs = await SharedPreferences.getInstance();
//  bool stayLoggedin = prefs.getBool('stayLoggedIn');
//  if (stayLoggedin == null) stayLoggedin = true; //Default
//  prefs.setBool('stayLoggedIn', stayLoggedin);
//  return stayLoggedin;
//}
}

class Login {
  static bool checkLoginToken(String response) {
    print("Respon: " + response);
    try {
      if (response.contains('salah')) {
        return false;
      } else if (response.contains('atau')) {
        return false;
      }
    } catch (exception) {
      print("Error Decoding Data, Token: $exception");
    }
    return true;
  }

  static Future<bool> getNewToken() async {
    bool isValid = await appAuth.login();
    return isValid;
  }

  static void showErrorLogin(BuildContext context) {
    print("Token tidak pas.");
    globals.petugas?.token = "";
    globals.Utility.showAlertPopup(
        context, "Info", "Silakan masuk lagi.\n" + globals.errorMessage);
  }

  static void showDemoDialog<T>({BuildContext context, Widget child}) {
    showDialog<T>(
      context: context,
      builder: (BuildContext context) => child,
    ).then<void>((T value) {});
  }
}
