import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:ciduk/lockedscreen/home.dart';
import 'package:ciduk/signin/signin.dart';
import 'package:ciduk/signin/auth_service.dart';

AuthService appAuth = new AuthService();

void main() async {
  var prefs = await SharedPreferences.getInstance();

  Widget _login = LoginPage(prefs: prefs);
  Widget _home = Home();
  // Set default home.
  Widget _defaultHome = _login;

  runApp(DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) => ThemeData(
            primarySwatch: Colors.blue,
            primaryColorBrightness: Brightness.light,
            brightness: brightness,
          ),
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter Login',
          theme: theme,
          home: _defaultHome,
          routes: <String, WidgetBuilder>{
            "/login": (BuildContext context) => _login,
            "/home": (BuildContext context) => _home,
          },
        );
      }));
}
