import 'dart:convert';

class Pelanggan {
  final String nama, telepon, nomormeteran, alamat;
  final bool sudahcatat;
  Pelanggan(
      {this.nama,
      this.telepon,
      this.nomormeteran,
      this.alamat,
      this.sudahcatat});
  factory Pelanggan.fromMap(Map<String, dynamic> map) {
    return Pelanggan(
        nama: map['nama_pelanggan'],
        telepon: map['nomor_telepon'],
        nomormeteran: map['nomor_meteran'],
        alamat: map['alamat'],
        sudahcatat: map['sudah_catat']);
  }
}

class DaftarPelanggan {
  final List<Pelanggan> pelanggan;
  DaftarPelanggan({this.pelanggan});
}

DaftarPelanggan jsonToDaftarPelanggan(String embuh) {
  Iterable l = json.decode(embuh);
  List<Pelanggan> pelanggan =
      l.map((model) => Pelanggan.fromMap(model)).toList();
  return DaftarPelanggan(pelanggan: pelanggan);
}

class CatatAir {
  final String nomormeteran;
  final int sampai;
  CatatAir(this.sampai, this.nomormeteran);
}

class ResponseCatat {
  final String nomormeteran;
  final int tahun, bulan, sampai;
  ResponseCatat({this.nomormeteran, this.tahun, this.bulan, this.sampai});
  factory ResponseCatat.fromMap(Map<String, dynamic> json) {
    return ResponseCatat(
      nomormeteran: json['nomormeteran'],
      tahun: json['tahun'],
      bulan: json['bulan'],
      sampai: json['bulan'],
    );
  }
}
